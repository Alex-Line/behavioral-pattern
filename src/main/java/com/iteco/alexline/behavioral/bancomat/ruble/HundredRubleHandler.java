package com.iteco.alexline.behavioral.bancomat.ruble;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class HundredRubleHandler extends RubleHandlerBase {

    protected final int value = 100;

    public HundredRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
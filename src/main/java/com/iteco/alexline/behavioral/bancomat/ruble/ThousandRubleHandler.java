package com.iteco.alexline.behavioral.bancomat.ruble;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class ThousandRubleHandler extends RubleHandlerBase {

    protected final int value = 1000;

    public ThousandRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
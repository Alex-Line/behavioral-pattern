package com.iteco.alexline.behavioral.bancomat;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.FiftyDollarHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.HundredDollarHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.TenDollarHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.ThousandDollarHandler;
import com.iteco.alexline.behavioral.bancomat.euro.*;
import com.iteco.alexline.behavioral.bancomat.ruble.*;

public class HomeWorkCashMachine extends Bancomat {

    private BanknoteHandler handler;

    public HomeWorkCashMachine() {
        this.handler = new FiftyRubleHandler(null);
        this.handler = new HundredRubleHandler(handler);
        this.handler = new TwoHundredRubleHandler(handler);
        this.handler = new FiveHundredRubleHandler(handler);
        this.handler = new ThousandRubleHandler(handler);
        this.handler = new FiveThousandRubleHandler(handler);

        this.handler = new TenEuroHandler(handler);
        this.handler = new FiftyEuroHandler(handler);
        this.handler = new HundredEuroHandler(handler);
        this.handler = new FiveHundredEuroHandler(handler);
        this.handler = new ThousandEuroHandler(handler);

        this.handler = new TenDollarHandler(handler);
        this.handler = new FiftyDollarHandler(handler);
        this.handler = new HundredDollarHandler(handler);
        this.handler = new ThousandDollarHandler(handler);
    }

    @Override
    public boolean validate(String banknote) {
        return handler.validate(banknote);
    }
}

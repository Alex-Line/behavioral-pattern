package com.iteco.alexline.behavioral.bancomat.ruble;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class FiveThousandRubleHandler extends RubleHandlerBase {

    protected final int value = 5000;

    public FiveThousandRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
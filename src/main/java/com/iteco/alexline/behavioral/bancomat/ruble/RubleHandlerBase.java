package com.iteco.alexline.behavioral.bancomat.ruble;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public abstract class RubleHandlerBase extends BanknoteHandler {

    protected RubleHandlerBase(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public boolean validate(String banknote) {

        String[] banknoteArray = banknote.split(" ");
        if (!banknoteArray[banknoteArray.length - 1].equalsIgnoreCase("ruble")
                && !banknoteArray[banknoteArray.length - 1].equalsIgnoreCase("rubles"))
            return super.validate(banknote);

        try {
            final int amount = Integer.parseInt(banknoteArray[0]);
            if (amount % 50 != 0) {
                System.out.println("Некоректная сумма! Сумма должна быть кратка как минимум 50 рублям. " +
                        "Попробуйте ввести ещё раз.");
                return false;
            }
            if ((amount / getValue()) == 0) {
                if (amount % getValue() == 0) return true;
                return super.validate(banknote);
            } else {
                final int banknoteQuantity = prepareBanknoteToDelivery(amount);
                final int rest = amount - banknoteQuantity * getValue();
                if (rest == 0) return true;
                return super.validate(rest + " rubles");
            }
        } catch (NumberFormatException e) {
            System.out.println(e);
            System.out.println("Некоректная сумма! Попробуйте ввести ещё раз.");
        }
        return false;
    }

    protected abstract int getValue();

    protected int prepareBanknoteToDelivery(int quantity) {
        final int result = quantity / getValue();
        System.out.println(String.format("К сумме на выдачу добавлено %d купюр номиналом %d.",
                result, getValue()));
        return result;
    }

}
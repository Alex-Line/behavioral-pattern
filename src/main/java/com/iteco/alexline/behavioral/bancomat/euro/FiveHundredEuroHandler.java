package com.iteco.alexline.behavioral.bancomat.euro;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class FiveHundredEuroHandler extends EuroHandlerBase {

    protected final int value = 500;

    public FiveHundredEuroHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
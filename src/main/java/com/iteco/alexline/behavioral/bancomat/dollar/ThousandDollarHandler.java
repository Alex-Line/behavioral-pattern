package com.iteco.alexline.behavioral.bancomat.dollar;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class ThousandDollarHandler extends DollarHandlerBase {

    protected int value = 1000;

    public ThousandDollarHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

}
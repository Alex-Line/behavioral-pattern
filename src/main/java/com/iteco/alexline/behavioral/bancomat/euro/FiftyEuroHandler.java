package com.iteco.alexline.behavioral.bancomat.euro;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class FiftyEuroHandler extends EuroHandlerBase {

    protected final int value = 50;

    public FiftyEuroHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
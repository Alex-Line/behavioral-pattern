package com.iteco.alexline.behavioral.bancomat.ruble;


import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

/**
 * FiftyRubleHandler.
 *
 * @author Ilya_Sukhachev
 */
public class FiftyRubleHandler extends RubleHandlerBase {

    protected int value = 50;

    public FiftyRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }
}

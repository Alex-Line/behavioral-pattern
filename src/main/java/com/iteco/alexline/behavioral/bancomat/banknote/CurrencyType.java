package com.iteco.alexline.behavioral.bancomat.banknote;

/**
 * CurrencyType.
 *
 * @author Ilya_Sukhachev
 */
public enum CurrencyType {
    EUR,
    USD,
    RUB
}

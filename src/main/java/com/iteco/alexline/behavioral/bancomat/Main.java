package com.iteco.alexline.behavioral.bancomat;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        Bancomat bancomat = new HomeWorkCashMachine();
        System.out.println(bancomat.validate("10010 dollars"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("19760 dollars"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("1 dollar"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");

        System.out.println(bancomat.validate("550 rubles"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("8563950 rubles"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("12345670 rubles"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");

        System.out.println(bancomat.validate("9876540 euros"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("9 euros"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("285750 euros"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println(bancomat.validate("2930 euros"));
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - -");
    }

}
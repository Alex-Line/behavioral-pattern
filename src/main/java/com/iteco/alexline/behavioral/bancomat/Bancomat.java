package com.iteco.alexline.behavioral.bancomat;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.FiftyDollarHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.HundredDollarHandler;
import com.iteco.alexline.behavioral.bancomat.dollar.TenDollarHandler;
import com.iteco.alexline.behavioral.bancomat.ruble.FiftyRubleHandler;

/**
 * Bancomat.
 *
 * @author Ilya_Sukhachev
 */
public class Bancomat {
    private BanknoteHandler handler;

    public Bancomat() {
        handler = new FiftyRubleHandler(null);
        handler = new TenDollarHandler(handler);
        handler = new FiftyDollarHandler(handler);
        handler = new HundredDollarHandler(handler);
    }

    public boolean validate(String banknote) {
        return handler.validate(banknote);
    }

}

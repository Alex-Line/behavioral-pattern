package com.iteco.alexline.behavioral.bancomat.euro;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class ThousandEuroHandler extends EuroHandlerBase {

    protected final int value = 1000;

    public ThousandEuroHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
package com.iteco.alexline.behavioral.bancomat.euro;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class HundredEuroHandler extends EuroHandlerBase {

    protected final int value = 100;

    public HundredEuroHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}
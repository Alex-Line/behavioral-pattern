package com.iteco.alexline.behavioral.bancomat.ruble;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class TwoHundredRubleHandler extends RubleHandlerBase {

    protected final int value = 200;

    public TwoHundredRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

}
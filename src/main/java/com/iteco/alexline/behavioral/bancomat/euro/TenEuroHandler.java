package com.iteco.alexline.behavioral.bancomat.euro;

import com.iteco.alexline.behavioral.bancomat.banknote.BanknoteHandler;

public class TenEuroHandler extends EuroHandlerBase {

    protected final int value = 10;

    public TenEuroHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return this.value;
    }

}